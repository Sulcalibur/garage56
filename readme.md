# Garage56

You can view the working copy [here](http://suljam.com/clients/garage56/) of the latest draft.

---

The whole site will work using the Bootstrap framework along with a couple of extra classes for the custom theme.

---

To give an image the white border just add the class `.img-border` to the `img`

Example:

```
<img src="http://suljam.com/clients/garage56/assets/img/xIMG_0875_sm.jpg.pagespeed.ic.WbgUI-1avx.jpg" alt="" class="col-md-6 img-border">
```

---

Blockquotes need to have a paragraph sitting inside the `blockquote` tag and the `blockquote` will need to have the class `.container` added to it.

Example:

```
<blockquote class="container">
  <p class="col-md-8 col-md-offset-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi, perspiciatis, fugit, optio voluptas rem consequatur assumenda cumque quia facilis aut omnis in laborum doloremque? Temporibus, consequuntur dolores.</p>
</blockquote>
```

---

Text columns can be added to make the text flow into either two or three columns that will automatically align the height and degrade down to two and the default of one column depending on the screen size.

For two columns just add the class `.column2` and for three columns `.column3`.

Example:

```
<p class="column2">consectetur adipisicing elit. Quasi, perspiciatis, fugit, optio voluptas rem consequatur assumenda cumque quia facilis aut omnis in laborum doloremque? Temporibus, consequuntur</p>

<p class="column3">consectetur adipisicing elit. Quasi, perspiciatis, fugit, optio voluptas rem consequatur assumenda cumque quia facilis aut omnis in laborum doloremque? Temporibus, consequuntur</p>
```

### fonts

The fonts used are from the Google Fonts library and are:
* **Neuton** (_at weight 400 and 400 italic_)
* **Nunito**

**Neuton** is used for headers and block-quotes and **Nunito** is used for paragraphs and buttons.



### Any Problems

Any problems or questions feel free to contact me via [email](mailto:sul@suljam.com), [twitter - @sulcalibur](http://twitter.com/sulcalibur) or Skype Username Sulcalibur.
