var gulp         = require('gulp'),
    jade         = require('gulp-jade'),
    prettify     = require('gulp-prettify'),
    sass         = require('gulp-sass'),
    minifyCSS    = require('gulp-minify-css'),
    // sourcemaps   = require('gulp-sourcemaps'),
    // uglify       = require('gulp-uglify'),
    plumber      = require('gulp-plumber'),
    browserSync  = require('browser-sync'),
    autoprefixer = require('gulp-autoprefixer'),
    uncss        = require('gulp-uncss-task'),
    watch        = require('gulp-watch')
    ;

  // Markup Tasks
  gulp.task('markup', function() {
    gulp.src('project/*.jade')
    .pipe(plumber())
    .pipe(jade())
    .pipe(prettify({indent_size: 2}))
    .pipe(gulp.dest('build/'));
  });

  // JavaScript Tasks
  gulp.task('scripts', function(){
    gulp.src('js/*.js')
    gulp.src('bower_components/modernizr/modernizr.js')
    .pipe(plumber())
    // .pipe(uglify())
    .pipe(gulp.dest('build/assets/js/'))
  });

  // Styles Tasks
  gulp.task('styles', function() {
    gulp.src('project/assets/styles/*.scss')
    .pipe(plumber())
    .pipe(sass({
      style: 'compress'
    }))
    .pipe(minifyCSS({keepBreaks:true}))
    // .pipe(sourcemaps.init())
    .pipe(autoprefixer())
    // .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('build/assets/css/'))
  });

  gulp.task('browser-sync', function () {
    var files = [
    'build/assets/css/**/*.css',
    'build/assets/img/**/*',
    'build/assets/js/**/*.js',
    'build/*.html'
    ];

    browserSync.init(files, {
      server: {
        baseDir: './build'
      }
    });
  });

  // Watch Task
  gulp.task('watch', function() {
    gulp.watch('project/**/*.jade', ['markup']);
    gulp.watch('project/assets/styles/**/*.scss', ['styles']);
  });

  // Default task to be run with `gulp`
  gulp.task('default', ['markup','styles','scripts','watch','browser-sync']);
